import {getRandomInt, makeGenreName, makeExclamation} from "./helper.js"

function setName() {
    let element = document.getElementById("genre");
    element.innerHTML = makeExclamation();
}

document.getElementById("new").onclick = setName;
setName()