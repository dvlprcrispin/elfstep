export { getRandomInt, makeGenreName, makeExclamation };
import { classes, suffixes, exclamations } from "./names.js";

function getRandomInt(min: number, max: number) {
  //nicked from mdn
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

function makeGenreName(): string {
  let firstBit = classes[getRandomInt(0, classes.length - 1)]
  let secondBit = suffixes[getRandomInt(0, suffixes.length - 1)]
  return firstBit + secondBit
}

function makeExclamation(): string {
  let genre = makeGenreName();
  let rand = getRandomInt(0, exclamations.length);
  let exclamation = exclamations[rand];
  return exclamation.replace("GENRE", genre);
}
