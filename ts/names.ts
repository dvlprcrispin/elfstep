export {classes, suffixes, exclamations}

var classes = [ // D&D Names
                        "Barbarian", "Bard", "Cleric", "Druid", "Fighter", "Monk", "Sorcerer", "Warlock", "Wizard", "Witch",
                        "Dwarf", "Elf", "Halfling", "Human", "Dragonborn", "Gnome", "Tiefling", "Goblin",
                        // Blades in the Dark names
                        "Cutter", "Hound", "Leech", "Lurk", "Slide", "Spider", "Whisper"
                    ];
var suffixes = ["step", " house", "-hop", "core", " and bass"];
var exclamations = ["Hot off Ye Bandcamp's bestselling, it's GENRE!"];