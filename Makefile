all: ts html

# does it all the time but it works ok
ts: ./ts/main.ts ./ts/helper.ts ./ts/names.ts
	tsc

html: ./static/index.html
	cp ./static/* ./public/

clean:
	rm ./public/*